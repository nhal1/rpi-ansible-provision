# Raspberry Pi provisioning with Ansible

A set of playbooks for provisioning Raspberry Pi SD card with Ansible

## Purpose
When experimenting with Raspberry Pi I often need to re-create a device from scratch or create many identical boot SD cards. 
This Ansible playbook does following:
1. Downloads the required Raspbian image
2. Mounts it to a local file system
3. Modifies the boot partition:
  - Enables SSH on the first boot
  - Configures WiFi to a predefined SSID and PSK
4. Modifies the root partition:
  - Modifies the hostname to the predefined one
  - Updates the authorized_keys file with the provided keys
  - Disables SSH password authentication
  - Configures the Avahi (zeroconf) daemon

## Prerequisites
### 1. Linux machine for running Ansible
Appears that Ansible doesn't run on Windows. If Linux machine is not available, Oracle virtualbox (tested on Windows) can help to run Ansible on a VM. 
- 1. Create Ubuntu VM (20.04 was tested)
- 2. Install packages:
```bash
    sudo apt update && sudo apt upgrade && sudo apt install gcc make perl ansible git vim
    sudo apt install exfat-fuse exfat-utils
    ansible-galaxy collection install ansible.posix
```
- Install guest additions (Ask Google how to do it)
- Configure USB passthrough for SDCARD adapter, check with lsblk that SDCARD appears in Linux as block device. (This)[https://forums.virtualbox.org/viewtopic.php?f=35&t=82639] might help, or just google it.

### 2. Create your secrets.json from secrets.json.example
```json
{
  "ssid": "Put Your wifi SSID here",
  "psk": "WPA encrypted password here",
  "ssh_pub_keys": [
    "ssh-rsa AA...... user@host",
    "ssh-rsa BB...... user@host"
  ]
}
```

### 3. Enter the desired hostname in inventory.yml
Currently this is set to 
```yaml
hostname: rpi
```
Please note that this will become a part of your Pi domain name on the local network with the help of Avahi daemon (For example this will allow you to connect to your Pi with ssh@rpi.local in the example above).

### 4. Check your SD card reader device name
Eject the SD card from the SD card reader and run 
```bash
lsblk
```
Then insert the SD card and repeat the command.
Not the new device that has appeared. 
In my case the device name was `sdg`, in your case it might be different.
Update the device name in the `inventory.yml`

With  the `inventory.yml` and `secrets.json` matching your needs, you are good to go.

## Preparing and burning the SD card
```bash
ansible-playbook -i inventory.yml playbook.yml 
```

